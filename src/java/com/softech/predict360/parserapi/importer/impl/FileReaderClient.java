package com.softech.predict360.parserapi.importer.impl;

import java.io.IOException;
import java.util.List;

import com.softech.predict360.parserapi.importer.FileImporter;


public class FileReaderClient {

	FileImporter importer;
	
	public FileReaderClient() {
		super();
		importer = ConcreteImporter.getNewInstance();
	}
	
	public List<?> fetchRecords() throws IOException{
		return importer.fetchRecords();
	}
}
