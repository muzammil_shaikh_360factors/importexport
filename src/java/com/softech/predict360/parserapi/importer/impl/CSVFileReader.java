package com.softech.predict360.parserapi.importer.impl;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import au.com.bytecode.opencsv.bean.CsvToBean;

import com.softech.predict360.parserapi.importer.vo.OGMUserImportVO;
import com.softech.predict360.parserapi.importer.vo.PredictEmployeeImportVO;
import com.softech.predict360.parserapi.importer.vo.PredictEmployeeQualificationImportVO;
import com.softech.predict360.parserapi.importer.vo.PredictQualificationImportVO;
import com.softech.predict360.parserapi.importer.vo.PredictUserImportVO;


public class CSVFileReader extends ConcreteImporter {
	private String fileName;
	private char separator = ',';
	private char quoteChar = '"';
	private char escapeChar = '\\';
	private int skipLine = 1;
	private boolean strictQuotes = false;
	private boolean ignoreLeadingWhiteSpace = true;
	private String[] columns;
	private String serviceName;
	private final Properties properties;
	

	public CSVFileReader(final Properties properties) {
		super();
		this.properties=properties;
		
	}

	private void setAttributes() throws IOException {
		fileName=properties.getProperty("file_name");
		if (null==fileName)
			throw new IOException("File Name not found in property file [Set file_name]");

		if (null==properties.getProperty("separator"))
			separator =',';
		else
			separator= String.valueOf(properties.getProperty("separator")).charAt(0);

		if (null==properties.getProperty("quoteChar"))
			quoteChar ='"';
		else
			quoteChar = String.valueOf(properties.getProperty("quoteChar")).charAt(0);

		if (null==properties.getProperty("escapeChar"))
			escapeChar ='\\';
		else
			escapeChar = String.valueOf(properties.getProperty("escapeChar")).charAt(0);

		if (null==properties.getProperty("skip_line"))
			skipLine =1;
		else
			skipLine = Integer.valueOf(properties.getProperty("skip_line"));

		if (null==properties.getProperty("strict_quotes"))
			strictQuotes =false;
		else
			strictQuotes = properties.getProperty("strict_quotes").equalsIgnoreCase("true");

		if (null==properties.getProperty("ignoreLeadingWhiteSpace"))
			ignoreLeadingWhiteSpace =true;
		else
			ignoreLeadingWhiteSpace =properties.getProperty("ignoreLeadingWhiteSpace").equalsIgnoreCase("true");
		
		String beanAttributes=properties.getProperty("bean_attributes");
		if (null==beanAttributes)
			// the fields to bind do in JavaBean
			 columns= new String[] {"SurveyBankId","SurveyId","SectionId","FrameworkId","QuestionId","QuestionText","QuestionType","QuestionTimeStamp"};
		else
			columns=beanAttributes.split(",");

		serviceName=properties.getProperty("service_name");
		if (null==serviceName)
			serviceName ="QuestionImport";
	}

	@Override
	public List fetchRecords() throws IOException {
		setAttributes();
		CSVReader csvReader = new CSVReader(new FileReader(fileName), separator, quoteChar, escapeChar, skipLine, strictQuotes, ignoreLeadingWhiteSpace);
		ColumnPositionMappingStrategy strat = new ColumnPositionMappingStrategy();

		if(serviceName.equalsIgnoreCase("PredictUserImport")) {
			strat.setType(PredictUserImportVO.class);
		} else if(serviceName.equalsIgnoreCase("OGMUserImport")) {
			strat.setType(OGMUserImportVO.class);
		} else if(serviceName.equalsIgnoreCase("PredictEmployeeImport")) {
			strat.setType(PredictEmployeeImportVO.class);
		} else if(serviceName.equalsIgnoreCase("PredictQualificationImport")) {
			strat.setType(PredictQualificationImportVO.class);
		} else if(serviceName.equalsIgnoreCase("PredictEmployeeQualificationImport")) {
			strat.setType(PredictEmployeeQualificationImportVO.class);
		}
		
		strat.setColumnMapping(columns);
		CsvToBean csv = new CsvToBean();
		return csv.parse(strat, csvReader);
	}

	/*
	public void readAndDisplayFile() throws IOException {
		CSVReader reader = new CSVReader(new FileReader(FILE_NAME));
		String [] nextLine;
		while ((nextLine = reader.readNext()) != null) {
			super.display(nextLine);
		}

	}
*/
}
