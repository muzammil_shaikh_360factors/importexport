package com.softech.predict360.parserapi.importer.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.softech.predict360.parserapi.importer.FileImporter;
import com.softech.predict360.parserapi.logging.PredictLogger;

public abstract class ConcreteImporter implements FileImporter {
	private static final String DEFAULT_PROPERTY_FILE_NAME="fileimport.properties";
	private static String propertyFileName;
	private static final String DEFAULT_FILE_TYPE="CSV";
	private static Properties properties;
	private final static Logger LOGGER = Logger.getLogger("ConcreteImporter");

	public static Properties getProperties() {
		return properties;
	}

	public static FileImporter getNewInstance(){
		try {
			PredictLogger.setup();
			LOGGER.setLevel(Level.INFO);
			
		} catch (IOException ioe) {
			ioe.printStackTrace();
			throw new RuntimeException("Problems with creating the log files");
		}

		loadPropertyFile();
		final String FILE_TYPE=properties.getProperty("file_type", DEFAULT_FILE_TYPE);
		if (FILE_TYPE.equalsIgnoreCase("CSV")){
			return new CSVFileReader(properties);
		} 
		else if (FILE_TYPE.equalsIgnoreCase("XML")) 
			return new XMLFileReader();
		else
			return null;
	}
	
	protected void display(String [] sArr){
		for (int i = 0; i < sArr.length; i++) {
			System.out.print(sArr[i]+"\t");
		}
		System.out.print("\n");
	}	

	public static void setPropertyFileName(String propertyFileName) {
		ConcreteImporter.propertyFileName = propertyFileName;
	}

	/**
	 * @param fileName
	 *            = filename.properties Read properties file.
	 * */
	private static void loadPropertyFile() {
		properties = new Properties();
		try {
			//URL url =  ClassLoader.getSystemResource(fileName);
			//properties.load(new FileInputStream(new File(url.getFile())));
			if(null!=propertyFileName)
				properties.load(new FileInputStream(propertyFileName));
			//else
			//properties.load(new FileInputStream(DEFAULT_PROPERTY_FILE_NAME));
		} catch (IOException e) {
			//throw new RuntimeException("Property file could not be loaded ",e);
		}

	}

}
