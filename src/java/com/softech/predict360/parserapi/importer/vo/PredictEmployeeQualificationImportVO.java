package com.softech.predict360.parserapi.importer.vo;

import java.io.Serializable;

public class PredictEmployeeQualificationImportVO implements Serializable {

	private static final long serialVersionUID = -5151060508327164298L;
	
	private String qualificationName;
	private String employeeName;
	private String qualifiedDate;
	private String expirationDate;
	private boolean saved;
	private boolean isUpdateCase;
	private String errMsg;
	
	public String getQualificationName() {
		return qualificationName;
	}
	public void setQualificationName(String qualificationName) {
		this.qualificationName = qualificationName;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getQualifiedDate() {
		return qualifiedDate;
	}
	public void setQualifiedDate(String qualifiedDate) {
		this.qualifiedDate = qualifiedDate;
	}
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	public boolean isSaved() {
		return saved;
	}
	public void setSaved(boolean saved) {
		this.saved = saved;
	}
	public boolean isUpdateCase() {
		return isUpdateCase;
	}
	public void setUpdateCase(boolean isUpdateCase) {
		this.isUpdateCase = isUpdateCase;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	@Override
	public String toString() {
		return "Qualification Name: " + qualificationName + ", Employee Name: " + employeeName + ", Qualified Date: " + qualifiedDate + ", Expiration Date: " + expirationDate;
	}

}
