package com.softech.predict360.parserapi.importer.vo;

import java.io.Serializable;


public class PredictEmployeeImportVO implements Serializable {

	private static final long serialVersionUID = 4284348481759925452L;
	
	private String employeeName;
	private String employeeNumber;
	private String emailAddress;
	private String description;
	private String username;
	private boolean saved;
	private boolean isUpdateCase;
	private String errMsg;
	
	
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getEmailAddress() {
		if(emailAddress!=null)
			return emailAddress.trim().toLowerCase();
		
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isUpdateCase() {
		return isUpdateCase;
	}
	public void setUpdateCase(boolean isUpdateCase) {
		this.isUpdateCase = isUpdateCase;
	}
	public boolean isSaved() {
		return saved;
	}
	public void setSaved(boolean saved) {
		this.saved = saved;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	@Override
	public String toString() {
		return "Name: " + employeeName + ", Number: " + employeeNumber + ", Email: " + emailAddress;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

}
