package com.softech.predict360.parserapi.importer.vo;

import java.io.Serializable;


public class OGMUserImportVO implements Serializable {

	private static final long serialVersionUID = 9066124660888020446L;

	private String firstName;
	private String lastName;
	private String emailAddress;
	private String password;
	private String userName;
	private String organizationalName;
	private String siteName;
	private String managerUsername;
	private String customerKey;
	private boolean saved;
	private String errMsg;
	
	public String getFirstName() {
		if(firstName!=null)
			return firstName.trim();
		
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		if(lastName!=null)
			return lastName.trim();
		
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailAddress() {
		if(emailAddress!=null)
			return emailAddress.trim().toLowerCase();
		
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserName() {
		if(userName!=null)
			return userName.trim().toLowerCase();
		
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getCustomerKey() {
		if(customerKey!=null)
			return customerKey.trim();

		return customerKey;
	}
	public void setCustomerKey(String customerKey) {
		this.customerKey = customerKey;
	}
	public String getSiteName() {
		if(siteName!=null)
			return siteName.trim();

		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getManagerUsername() {
		if(managerUsername!=null)
			return managerUsername.trim();

		return managerUsername;
	}
	public void setManagerUsername(String managerUsername) {
		this.managerUsername = managerUsername;
	}
	public String getOrganizationalName() {
		if(organizationalName!=null)
			return organizationalName.trim();

		return organizationalName;
	}
	public void setOrganizationalName(String organizationalName) {
		this.organizationalName = organizationalName;
	}
	public boolean isSaved() {
		return saved;
	}
	public void setSaved(boolean saved) {
		this.saved = saved;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
}
