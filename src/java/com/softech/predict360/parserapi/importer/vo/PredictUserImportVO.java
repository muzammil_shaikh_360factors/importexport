package com.softech.predict360.parserapi.importer.vo;

import java.io.Serializable;


public class PredictUserImportVO implements Serializable {

	private static final long serialVersionUID = -2045856713942297796L;

	private String firstName;
	private String lastName;
	private String emailAddress;
	private String password;
	private String userGroupList;
	private String roleList;
	private String userName;
	private String resellerKey;
	private String customerKey;
	private String allowLMS;
	private String allowCM;
	private String allowDMS;
	private String allowPMS;
	private boolean saved;
	private boolean isUpdateCase;
	private String errMsg;
	
	public String getFirstName() {
		if(firstName!=null)
			return firstName.trim();
		
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		if(lastName!=null)
			return lastName.trim();
		
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailAddress() {
		if(emailAddress!=null)
			return emailAddress.trim().toLowerCase();
		
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserGroupList() {
		if(userGroupList!=null)
			return userGroupList.trim();

		return userGroupList;
	}
	public void setUserGroupList(String userGroupList) {
		this.userGroupList = userGroupList;
	}
	public String getUserName() {
		if(userName!=null)
			return userName.trim().toLowerCase();
		
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getCustomerKey() {
		if(customerKey!=null)
			return customerKey.trim();

		return customerKey;
	}
	public void setCustomerKey(String customerKey) {
		this.customerKey = customerKey;
	}
	public String getResellerKey() {
		if(resellerKey!=null)
			return resellerKey.trim();

		return resellerKey;
	}
	public void setResellerKey(String resellerKey) {
		this.resellerKey = resellerKey;
	}
	public boolean isSaved() {
		return saved;
	}
	public void setSaved(boolean saved) {
		this.saved = saved;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public String getRoleList() {
		return roleList;
	}
	public void setRoleList(String roleList) {
		this.roleList = roleList;
	}
	public String getAllowLMS() {
		if(allowLMS==null)
			return "NO";
		return allowLMS.trim();
	}
	public void setAllowLMS(String allowLMS) {
		this.allowLMS = allowLMS;
	}
	public String getAllowCM() {
		if(allowCM==null)
			return "NO";
		return allowCM.trim();
	}
	public void setAllowCM(String allowCM) {
		this.allowCM = allowCM;
	}
	public String getAllowDMS() {
		if(allowDMS==null)
			return "NO";
		return allowDMS.trim();
	}
	public String getAllowPMS() {
		if(allowPMS==null)
			return "NO";
		return allowPMS.trim();
	}
	public void setAllowPMS(String allowPMS) {
		this.allowPMS = allowPMS;
	}
	public void setAllowDMS(String allowDMS) {
		this.allowDMS = allowDMS;
	}
	public boolean isUpdateCase() {
		return isUpdateCase;
	}
	public void setUpdateCase(boolean isUpdateCase) {
		this.isUpdateCase = isUpdateCase;
	}

}
