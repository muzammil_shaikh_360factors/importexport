package com.softech.predict360.parserapi.importer.vo;

import java.io.Serializable;


public class PredictQualificationImportVO implements Serializable {

	private static final long serialVersionUID = 4284348481759925452L;
	
	private String name;
	private String renewalDuration;
	private String renewalFrequency;
	private String compulsory;
	private String compulsoryOther;
	private boolean saved;
	private boolean isUpdateCase;
	private String errMsg;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRenewalDuration() {
		return renewalDuration;
	}
	public void setRenewalDuration(String renewalDuration) {
		this.renewalDuration = renewalDuration;
	}
	public String getRenewalFrequency() {
		return renewalFrequency;
	}
	public void setRenewalFrequency(String renewalFrequency) {
		this.renewalFrequency = renewalFrequency;
	}
	public String getCompulsory() {
		return compulsory;
	}
	public void setCompulsory(String compulsory) {
		this.compulsory = compulsory;
	}
	public String getCompulsoryOther() {
		return compulsoryOther;
	}
	public void setCompulsoryOther(String compulsoryOther) {
		this.compulsoryOther = compulsoryOther;
	}
	public boolean isUpdateCase() {
		return isUpdateCase;
	}
	public void setUpdateCase(boolean isUpdateCase) {
		this.isUpdateCase = isUpdateCase;
	}
	public boolean isSaved() {
		return saved;
	}
	public void setSaved(boolean saved) {
		this.saved = saved;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	@Override
	public String toString() {
		return "Name: " + name + ", Duration: " + renewalDuration + ", Frequency: " + renewalFrequency + ", Is Compulsory: " + compulsory;
	}

}
