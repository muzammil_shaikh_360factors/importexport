package com.softech.predict360.parserapi.importer;

import java.io.IOException;
import java.util.List;

public interface FileImporter {
	List fetchRecords() throws IOException ;
}
