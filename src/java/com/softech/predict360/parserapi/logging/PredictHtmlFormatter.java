package com.softech.predict360.parserapi.logging;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;


//This custom formatter formats parts of a log record to a single line
class PredictHtmlFormatter  extends Formatter
{
	// This method is called for every log records
	public String format(LogRecord rec)
	{
		StringBuffer buf = new StringBuffer(1000);
		// Bold any levels > WARNING

		if (rec.getLevel().intValue() > Level.WARNING.intValue())
		{
			buf.append("<b>");
			buf.append("<tr style='color: red; font-weight: bold;' onmouseover=\"this.style.backgroundColor='#ffff66';\" onmouseout=\"this.style.backgroundColor='#d4e3e5';\">");
			buf.append("<td>");
				buf.append(rec.getLevel());
			buf.append("</td>");
			buf.append("<td>");
				buf.append(calcDate(rec.getMillis()));
			buf.append("</td>");
			buf.append("<td>");
				buf.append(formatMessage(rec));
			buf.append('\n');
			buf.append("</td>");
			buf.append("</tr>");
			buf.append("</b>\n");
		} /*else
		{
			buf.append("<tr>");
			buf.append("<td>");
			buf.append(rec.getLevel());
			buf.append("</td>");
			buf.append("<td>");
			buf.append(calcDate(rec.getMillis()));
			buf.append("</td>");
			buf.append("<td>");
			buf.append(formatMessage(rec));
			buf.append('\n');
			buf.append("</td>");
			buf.append("</tr>\n");
		}*/
		return buf.toString();
	}

	private String calcDate(long millisecs)
	{
		SimpleDateFormat date_format = new SimpleDateFormat("MMM dd,yyyy HH:mm");
		Date resultdate = new Date(millisecs);
		return date_format.format(resultdate);
	}

	// This method is called just after the handler using this
	// formatter is created
	public String getHead(Handler h)
	{
		return "<HTML>\n<HEAD>\n" 
		+"<!-- CSS goes in the document HEAD or added to your external stylesheet -->"
		+"<style type=\"text/css\">\n"
		+"table.hovertable {\n"
			+"font-family: verdana,arial,sans-serif;\n"
			+"font-size:11px;\n"
			+"color:#333333;\n"
			+"border-width: 1px;\n"
			+"border-color: #999999;\n"
			+"border-collapse: collapse;\n"
		+"}\n"
		+"table.hovertable th {\n"
			+"background-color:#c3dde0;\n:"
			+"border-width: 1px;\n"
			+"padding: 8px;\n"
			+"border-style: solid;\n"
			+"border-color: #a9c6c9;\n"
		+"}\n"
		+"table.hovertable tr {\n"
			+"background-color:#d4e3e5;\n"
		+"}\n"
		+"table.hovertable td {\n"
			+"border-width: 1px;\n"
			+"padding: 8px;\n"
			+"border-style: solid;\n"
			+"border-color: #a9c6c9;\n"
		+"}\n"
		+"</style>\n"

		//+ (new Date()) 
		+ "\n</HEAD>\n<BODY>\n<PRE>\n"
				+ "<table class=\"hovertable\">\n  "
				+ "<tr><th>Level</th><th>Time</th><th>Log Message</th></tr>\n";
	}

	// This method is called just after the handler using this
	// formatter is closed
	public String getTail(Handler h)
	{
		return "</table>\n  </PRE></BODY>\n</HTML>\n";
	}
} 
