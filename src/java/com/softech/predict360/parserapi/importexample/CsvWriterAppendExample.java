package com.softech.predict360.parserapi.importexample;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import au.com.bytecode.opencsv.CSVWriter;

public class CsvWriterAppendExample {
	public static void main(String[] args) {
		
		String outputFile = "C:/exampleQuestions.csv";
		
		// before we open the file check to see if it already exists
		boolean alreadyExists = new File(outputFile).exists();
			
		try {
			// use FileWriter constructor that specifies open for appending
			CSVWriter csvOutput = new CSVWriter(new FileWriter(outputFile, true), ',');
			List<String[]> lstValues=new ArrayList<String[]>(6);
			// if the file didn't already exist then we need to write out the header line
			if (!alreadyExists)
			{
				lstValues.add(new String[]{"surveyId","sectionId","questionBankId","questionId","text"});
				csvOutput.writeAll(lstValues);
			}
			// else assume that the file already has the correct header line
			
			// write out a few records
			csvOutput.writeNext(new String[]{"1","1","1","10","what is your city\n\r name?"});
			csvOutput.writeNext(new String[]{"1","1","2","11","wha't \"i,s\" ,	you			r pe\t name?"});csvOutput.writeNext(new String[]{"1","1","2","11","***************************"});
			
			csvOutput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
